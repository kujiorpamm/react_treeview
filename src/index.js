import 'core-js/es6/map';
import 'core-js/es6/set'
import 'raf/polyfill';
import React from 'react';
import { render} from 'react-dom';
import App from './components/app.js';
import './styles.less';
import "babel-polyfill";;
// export default App;


// url, container_id
window.initTreeView = function(_config, container_id) {
    render(<App config={_config}/>, document.getElementById(container_id));
}

render(<App/>, document.getElementById('root'));
