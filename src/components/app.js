import React, {Component} from 'react';
import Draggable from 'react-draggable';
import MenuItem from './menu-item';
import axios from 'axios';
import data from '../data.json';
import { StickyContainer, Sticky } from 'react-sticky';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_loaded: false,
            items: [],
            _config: this.props.config,
            bounds: {right: 0, left:  0 },
            names: [],
            position: {}
        }

        this.handleStop = this.handleStop.bind(this);

    }

    handleStop(data, data2) {
        document.getElementsByClassName('column-names')[0].style.transform = "translate(" + data2.x + "px)";
        // this.setState({
        //     position: {x: data2.x, y: 0}
        // })
    }

    componentDidMount() {
        this.setState({
            config: this.props.config,
            bounds: {
                right: 0,
                left: document.getElementsByClassName('waka-container')[0].offsetWidth - 1541
            }
        });


        let url = this.props.config.url;

        axios.get(url)
          .then(res => {
            this.setState({
                items: res.data.items,
                names: res.data.names
            });
          })


    }



    render() {
        // console.log(this.state.bounds);
        let names = this.state.items.map((item) => {
            return <MenuItem mode="names" model={item.model} name={item.name} items={item.children} key={Math.random()} />
        });
        let values = this.state.items.map((item) => {
            return <MenuItem mode="values" model={item.model} name={item.name} items={item.children} key={Math.random()} />
        });
        return (

            <div className="treeview">
                <StickyContainer>

                    <div className="names-container">
                        <Sticky>
                            {({
                                style,
                                isSticky,
                                wasSticky,
                                distanceFromTop,
                                distanceFromBottom,
                                calculatedHeight
                              }) => (
                                <div style={style} className={"st-names " + isSticky}>
                                    <div className="column-names">
                                    {
                                        this.state.names.map((item) => {
                                            return (<div className="item" key={Math.random()}> {item} </div>);
                                        })
                                    }
                                    </div>

                                </div>
                            )}
                        </Sticky>
                    </div>

                    <div className="tableview">
                        <div className="left"> {names} </div>
                        <div className="right waka-wrap">
                            <div className="waka-container">
                                <Draggable
                                    axis="x"
                                    handle=".handle"
                                    bounds={this.state.bounds}
                                    // onStart={this.handleStart}
                                    onDrag={this.handleStop}
                                    onStop={this.handleStop}>
                                    <div className="handle">
                                        {values}
                                    </div>
                                  </Draggable>
                            </div>
                        </div>
                    </div>
                    </StickyContainer>
            </div>
        );

    }

}
