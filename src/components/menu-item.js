import React, {Component} from 'react';

export default class MenuItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.name,
            items: this.props.items,
            mode: this.props.mode,
            active: false,
            values: [0]
        }
        this.toggleClass = this.toggleClass.bind(this);
    }

    numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        return parts.join(".");
    }

    componentDidMount() {
        if(this.props.model) {
            let model = this.props.model[0];
            this.setState({
                values: [model.budget_approved, model.budget_refined, model.budget_corrected, model.plan_payments, model.plan_obligations, model.obligations_accepted, model.obligations_unpaid, model.budget_execution, model.budget_execution_percent, model.budget_execution_percent_total]
            });
        }

        let _parent_nodes = document.getElementsByClassName('child-' + this.props.model[0].id);
         // console.log(_parent_nodes[0]);
        let _sizes = [0];
        Array.from(document.getElementsByClassName('child-' + this.props.model[0].id)).forEach(function(item) {
            _sizes.push(item.clientHeight);
        });

        let _max_size = Math.max.apply(null, _sizes);

        for(var i = 0; i < _parent_nodes.length; i++) {
            _parent_nodes[i].style.height = _max_size + 1 + 'px';
        }



    }

    toggleClass() {
        const currentState = this.state.active;
        this.setState({ active: !currentState });
    };

    renderNames() {
        let items = false;
        if(this.state.items) {
            items = this.state.items.map((item) => {
                return <MenuItem mode={this.state.mode} model={item.model} name={item.name} items={item.children}  key={Math.random()}/>
            });
        }

        let cn = "item-name child-" + this.props.model[0].id;
        return (
            <div className="item-holder">
                <div className={cn}>{this.state.name}</div>
                {items}
            </div>

        );
    }
    renderValues() {
        let items = false;
        if(this.state.items) {
            items = this.state.items.map((item) => {
                return <MenuItem mode={this.state.mode} model={item.model} name={item.name} items={item.children}  key={Math.random()}/>
            });
        }

        let cn = "item-data-container child-" + this.props.model[0].id;
        let cn2 = "item-data";
        let data_items = this.state.values.map((value) => {
            return <div className={cn2}>{this.numberWithCommas(value)}</div>
        });
        return (
            <div className="item-holder">
                <div className={cn}>{data_items}</div>
                {items}
            </div>

        );
    }

    render() {

        if(this.state.mode == "names") return this.renderNames();
        if(this.state.mode == "values") return this.renderValues();

        return this.renderNames();

        let data_items = this.state.values.map((value) => {
            return <div className="item-data">{value}</div>
        });

        // Если есть наследники
        if(this.state.items && this.state.items.length > 0) {
            let items = this.state.items.map((item) => {
                return <MenuItem mode={this.state.mode} model={item.model} name={item.name} items={item.children}  key={Math.random()} />
            });
            if(this.state.mode == "names") {
                let class_name = "item has-childs child-" + this.props.model[0].id;
                return (
                    <div className={class_name}>
                        <div className="data-holder">
                            <div onClick={this.toggleClass} className="name">{this.state.name}</div>
                        </div>
                        <div className="childs">
                            {items}
                        </div>
                    </div>
                );
            } else if(this.state.mode == "values") {
                let class_name = "item has-childs child-" + this.props.model[0].id;
                return (
                    <div className={class_name}>
                        <div className="data-holder">
                            {data_items}
                        </div>
                        <div className={this.state.active ? 'childs active': 'childs'} >
                            {items}
                        </div>
                    </div>
                );
            }

        }
        // Если нет наследников
        else {

            let class_name = "item child-" + this.props.model[0].id;
            if(this.state.mode == "names") {
                return (
                    <div className={class_name}>
                        <div className="data-holder">
                            <div onClick={this.toggleClass} className="name">{this.state.name}</div>
                        </div>
                    </div>
                );
            } else if(this.state.mode == "values") {
                return (
                    <div className={class_name}>
                        <div className="data-holder">
                            {data_items}
                        </div>
                    </div>
                );
            }

        }
    }

}
